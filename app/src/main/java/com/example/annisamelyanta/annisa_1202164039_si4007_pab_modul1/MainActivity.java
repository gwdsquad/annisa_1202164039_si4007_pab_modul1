package com.example.annisamelyanta.annisa_1202164039_si4007_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText p,l;
    Button hitung;
    TextView hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        p = (EditText)findViewById(R.id.panjang);
        l = (EditText)findViewById(R.id.lebar);
        hitung = (Button) findViewById(R.id.cek);
        hasil = (TextView)findViewById(R.id.luas);

        hitung.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String pjg = p.getText().toString();
                String lbr = l.getText().toString();

                double p = Double.parseDouble(pjg);
                double l = Double.parseDouble(lbr);
                double hs = LuasLahan(p,l);

                String output = String.valueOf(hs);
                hasil.setText(output.toString());
            }
        });
    }
    public double LuasLahan(double p, double l){return p*l;}
}